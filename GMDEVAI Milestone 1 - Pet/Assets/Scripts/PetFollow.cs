﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetFollow : MonoBehaviour
{
    public Transform owner;
    public float speed = 3;
    public float rotSpeed = 3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 lookAtOwner = new Vector3(owner.position.x,
                                        this.transform.position.y,
                                        owner.position.z);

        Vector3 direction = lookAtOwner - transform.position;
        
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                Quaternion.LookRotation(direction),
                                                Time.deltaTime * rotSpeed);

        if (Vector3.Distance(lookAtOwner,transform.position) > 1)
        {
            transform.Translate(0,0,speed * Time.deltaTime);
        }
    }
}
