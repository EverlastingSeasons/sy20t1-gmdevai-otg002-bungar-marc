﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public GameObject explosion;
	public int damage;
	
	void OnCollisionEnter(Collision col)
    {
    	GameObject e = Instantiate(explosion, this.transform.position, Quaternion.identity);
    	Destroy(e,1.5f);
    	Destroy(this.gameObject);

        if (col.gameObject.tag=="Player")
        {
			col.gameObject.GetComponent<Player>().TakeDamage(damage);
        }

        if (col.gameObject.tag=="Enemy")
        {
			col.gameObject.GetComponent<TankAI>().TakeDamage(damage);
        }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
