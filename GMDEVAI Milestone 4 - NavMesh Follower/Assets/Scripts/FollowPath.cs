﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour
{
    public UnityEngine.AI.NavMeshAgent agent;
    public GameObject wpManager;
    GameObject[] wps;

    // Start is called before the first frame update
    void Start()
    {
        wps = wpManager.GetComponent<WaypointManager>().waypoints;
        agent = this.GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    public void GoToHelipad()
    {
        //graph.AStar(currentNode,wps[0]);
        //currentWaypointIndex = 0;
        agent.SetDestination(wps[0].transform.position);
    }

    public void GoToRuins()
    {
        //graph.AStar(currentNode,wps[6]);
        //currentWaypointIndex = 0;
        agent.SetDestination(wps[6].transform.position);
    }

    public void GoToFactory()
    {
        //graph.AStar(currentNode,wps[7]);
        //currentWaypointIndex = 0;
        agent.SetDestination(wps[7].transform.position);
    }
}
